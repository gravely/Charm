Charm
=====
Charm is a kitchen sink of approaches to naively find intersting files.

It was written to fulfill a very specific purpose because nothing was available
at the time: walk a directory of files (or MIME email bodies) as part of an Exim
SMTP filter, find attachments that bear a deeper look, archive them somewhere,
and send an e-mail detailing the findings.

Charm can snoop inside of common archive types and can walk e-mail attachments
including attachments in attachments in attachments, etc.

Usage
=====
    charm.py (options) [file to Scan]

    Options:
      --version             show program's version number and exit
      -h, --help            show this help message and exit
      -v, --verbose         verbose output
      -r RECIPIENT, --recipient=RECIPIENT
                            send Report email to RECIPIENT
      -m SERVER, --mta=SERVER
                            use SERVER as MTA
      -e, --exim            look for and process exim queue files
      -S, --syslog          Write to syslog
      -F FACILITY, --facility=FACILITY
                            syslog facility. Defaults: 'user')
      -p PRIORITY, --priority=PRIORITY
                            syslog priority. Defaults: 'info'